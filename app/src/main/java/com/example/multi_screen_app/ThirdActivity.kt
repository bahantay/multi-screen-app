package com.example.multi_screen_app

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class ThirdActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.third_activity)

        val goForwardBtn = findViewById<Button>(R.id.forwardBtn)
        val goBackBtn = findViewById<Button>(R.id.backBtn)

        goForwardBtn.setOnClickListener {
            val fourthIntent = Intent(this, FourthActivity::class.java)
            startActivity(fourthIntent)
        }

        goBackBtn.setOnClickListener {
            finish()
        }
    }
}